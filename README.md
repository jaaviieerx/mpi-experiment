# Parallel vs Serial #

To test parallel and serial codes against local processor, on single or multicore. 

### What is the environment used? ###

* open-mpi: stable 4.0.5 (bottled), HEAD
* clang 11.0.3
* VSCode: 1.50.1
* 3.1 GHz Core i7 (I7-5557U)
* MacOS Catalina 10.15.7

### What for? ###

* Coursework 2 for IT204 Hardware