#include <time.h>
#include <mpi.h>
#include <omp.h>
#include <stdio.h>
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv) {
    MPI_Init(NULL, NULL);
    // start stopwatch
    clock_t tStart = clock();
    printf("Codes initialised\n");
    int rank, world;
    // process inside a communicator, default MPI_COMM_WORLD
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    // total number of ranks in this communicator
    MPI_Comm_size(MPI_COMM_WORLD, &world);
    // initialise output file with its headers
    ofstream outputFile("thparallel.csv");
    outputFile << "Workload,Processor,Total Processors" << endl;
    // begin code within MPI
    printf("Codes Architecture: Parallel-to-Threads, Total Processors: %d\n", world);
    #pragma omp parallel for collapse(4)
    for (int i=0; i<100000; i++) {
        //cout << i << ", Rank: " << rank << endl;
        outputFile << i << "," << rank << "," << world << endl;
    }
    outputFile.close();
    MPI_Finalize();
    // end stopwatch
    printf("Codes exited in: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
    return 0;
}
// mpic++ thparallel.cpp -o thparallel
// mpirun -n 4 --use-hwthread-cpus thparallel